Emacs
=====

> I've tried, really, it's not gonna work... Working with Emacs is like typing
> with left hand, your eyes covered, and sitting back to the desk...

Example of my config file: (examples/emacs/.emacs)

Make emacs editor for humans again
----------------------------------

### Copy to X clipboard

If you are using emacs in terminal you need to use `xclip`.

```sh
sudo pacman --sync xclip
```    

Install emacs package - `M-x package-list-packages`, select `xclip` (look up).
In ~/.emacs (package manager will add own shit to ~/.emacs, add yours after)

```elips
(xclip-mode 1)
```

Spell check
-----------

To enable on the fly spell checking:

`M-x` : `flyspell-mode`

To change spell dictionary:

`M-x` > `ispell-change-dictionary` : _type_the_lang_

On the fly spell check for comments and strings only:

`M-x` > `flyspell-prog-mode`

### correct current word

`M-$`

But, better to use flyspell popup menu invoked by command `flyspell-correct-word-before-point`

to bind this command to a key (F8 in the example), add this to '~/.emacs'

```elips
(global-set-key (kbd "<f8>") 'flyspell-correct-word-before-point)
```

Check spell in whole buffer `flyspell-buffer`.

### init flyspell mode

Key binding: Shift + F8

Add to ~/.emacs

```elips
(defun init-flyspell ()
  (interactive)
  (flyspell-mode)
  (flyspell-buffer))
(global-set-key (kbd "S-<f8>") 'init-flyspell)
```

Fuzzy matching
--------------

Instal package "ivy", to autostart `ivy-mode` add to '~/.emacs'

```elips                                                                                                                       
(ivy-mode 1)
```

> ivy fuzzy matching works a bit different that sublime - use space to match words.

Smooth scrolling
----------------

Add to ~/.emacs

```elips
(setq scroll-step 1)
```

Smart parentheses and quotation marks
-------------------------------------

Add to ~/.emacs

```elips
(electric-pair-mode)
(show-paren-mode)
```

Syntax checking / linting
-------------------------

Install *flycheck*. Then install language specific tools like `rubocop`, `jslint`.


Autoload add to ~/.emcas:

```elips
(add-hook 'after-init-hook #'global-flycheck-mode)
```

Key bindings (default)
----------------------

C- : Control
M- : Left Alt

* C-x C-c : exit
* C-x C-f : opens or creates a file
* C-x C-s : saves a file
* C-x s   : saves all files
* C-x C-w : Save as

### Copy / Cut / Paste

* C-k     : cut from cursor to EOL
* C-y     : paste
* C-space : starts marking text
* M-w     : copy marked text
* C-w     : cut market text

### Common

* C-g     : quit current command (e.g. marking text)
* C-/     : Undo
* C-g C-/ : Redo

### Text navigation

* M->   : Go to end of buffer
* M-<   : go to end of buffer
* C-s   : search as you type (forward), hit many times to go to another occurance
* M-C-s : regexp search (C-s : next occurance)
* C-%   : search and replace
* M-s o : search and show all the occurances
* M-x grep  : grep a pattern in the files
* M-x rgrep : recursive grep

### Window nawigation

* C-x o : select another window
* C-x 0 : close current window
* C-x 1 : close other windows
* C-x 2 : split window horizontally
* C-x 3 : split window vertically

### Buffer management

* C-x b ... : Switch to a buffer (or create one)
* C-x ←/→ : select previous/next buffer
* C-x C-b : Switch buffers via list
* C-x k   : Kill current buffer (or type buffer name to kill)


