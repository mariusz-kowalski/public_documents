Qemu on Arch with Arch
======================

Qemu with KVM virtualization with Arch as a guest and Arch as a host.

Create image
------------

On btrfs filesystem first create an empty file and give it No_COW flag. I'm
using *qcow2* image format, size of 5GB, qcow2 is dynamically allocated.

```
touch image.qcow2
chattr +C image.qcow2
qemu-img create -f qcow2 image.qcow2 5G
```

Install guest OS
----------------

I'm using network configuration
[Qemu systemd-networkd](qemu-systemd-networkd.md)

> Set 512MB of ram, Arch installation media won't boot with less memory.

```
sudo qemu-system-x86_64 -enable-kvm -m 512 \
-cdrom ~/Downloads/archlinux-2016.12.01-dual.iso -boot order=d \
-net nic,model=virtio -net bridge,br=qemu0 \
-drive file=image.qcow2,if=virtio
```

Qemu will create VNC session with the machine. `vncviewer localhost:5900`.
Install *tigervnc* if you don't have the client.

Proceed with normal Arch installation with few exceptions...

@ToDo point out the exceptions

### Kernel

In file */etc/mkinitcpio.conf*

Config the modules:

```
MODULES="virtio virtio_blk virtio_pci virtio_net"
```

Add btrfs hook:

```
HOOKS="base udev autodetect modconf block filesystems btrfs keyboard fsck"
```

### Network

Create file */etc/systemd/network/wired.network*, put name of **your** network
device.

```
[Match]
Name = ens3

[Network]
DHCP=ipv4
```

```sh
ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
systemctl enable systemd-networkd
systemctl enable systemd-resolved
```

Change in */etc/systemd/network/wired.network* line with **host: ** to

```
hosts: files resolve myhostname
```

### Install ssh

Install ssh, allow root login, enable the service.

### Qemu guset agent

```
pacman --sync qemu-guest-agent
systemctl enable qemu-ga.service
```

### Give it a name

Put hostname in */etc/hostname*, your machine will be available by this name, if
both guest and host use systemd-networkd and systemd-resolved.

Run the machine
---------------

```
qemu-system-x86_64 -enable-kvm -m 512 -nographic \
-boot order=c -drive file=image.qcow2,if=virtio \
-net nic,model=virtio -net bridge,br=qemu0 \
```

> `sudo` is unnecessary

Ephemeral machine
-----------------

It's done by using overlay image.

```
qemu-img create -o backing_file=image.qcow2,backing_fmt=qcow2 \
         -f qcow2 overlay_img.qcow2
```

Then run machine with 'overlay_img.qcow2' as the drive.
