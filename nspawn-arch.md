Create systemd-nspawn container
===============================

pacstrap minimal installation
-----------------------------

It might be possible to shrink it even more...

Do it in */var/lib/machines*.

```bash
sudo btrfs subvolume create _name_

sudo pacstrap -c -i -d _name_ \
  bash bzip2 coreutils dhcpcd diffutils file filesystem findutils gawk \
  gcc-libs gettext glibc grep gzip inetutils iproute2 iputils less logrotate \
  nano netctl pacman perl procps-ng psmisc s-nail sed shadow sysfsutils \
  systemd-sysvcompat tar texinfo util-linux which bash-completion
```

Add "pts/0" to /etc/securetty (in the container) to fix login issue.

unit container config
---------------------

File /etc/systemd/nspawn/_name_.nspawn

```
[Exec]
PrivateUsers = No

[Files]
Bind = /host/dir:/container/dir

[Network]
Private = No
VitrualEthernet = No
```

This will mount /host/dir inside the container, on /container/dir inside the
container. Container network will be shared with hosts one. User id will be
also shared with host ids.

If you need to run multiple instances of same machine, clones provide same
service, like SQL database, better solution is to configure virtual network
for containers. That way it is not needed to change configuration of the
service, like setup new port for sql server, instead just configure new
connection (host name, which is container name) in the app...

Remark: To allow containers to revolve each others names they must be in the
same network, containers must be connected to virtual bridge. Systemd can
manage this automatically by zones. Zone is a bridge created each time container
from zone is booted.

File /etc/systemd/nspawn/_name_.nspawn

```
[Exec]
PrivateUsers = No

[Files]
Bind = /host/dir:/container/dir

[Network]
Zone=_zone_name_
```

Inside the container
--------------------

### network

> In fact this should be done on the host as well

In file */etc/nsswitch.conf* change line `hosts: ...` to

```
hosts: files resolve myhostname
```

```sh
sudo systemctl enable systemd-networkd
sudo systemctl enable systemd-resolved

sudo rm /etc/resolv.conf
sudo ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
```
