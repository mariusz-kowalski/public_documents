; Go to line command
; (global-set-key (kbd "C-c j") 'goto-line)

(add-to-list 'load-path "~/.emacs.d/vendor")

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (xclip))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

; paste primary selection or clipboard with Shift + middle_button
; there are two types of selection:
; * with Shift - it's terminal selection (del, bksp, ctrl+x wont work)
; * normal - it's emacs selection - it wont update X primary selection
(xclip-mode 1)
(xterm-mouse-mode)

(setq make-backup-files nil)

; Ctrl + x/c/v when something is selected 
(cua-mode)

; spell check sugestions
(global-set-key (kbd "<f8>") 'flyspell-correct-word-before-point)

