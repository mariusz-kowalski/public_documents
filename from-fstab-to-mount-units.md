From fstab to mount units
=========================

generate mount unit files from current */etc/fstab*

```sh
mkdir normal early late
/usr/lib/systemd/system-generators/systemd-fstab-generator normal early late
```

It will generate unit files and put then in those three dirs.
You can copy some of if to */etc/systemd/system* dir and remove entries form
*fstab* file.

This is not recommended. If your system has to be 99,99999% stable I would not
do this.

Also I'm not replacing */*, */boot* and *swap* entries (I'm leaving then in
*fstab*).

Fixing units
------------

Remove unnecessary stuff from unit files, comments, *SourcePath*, *Documentation*
entries. Add *Description* entry in [Unit] section. You can remove *Before*
entry (it will be replaced by Install section).

Add section [Install] and add there one of entry WantedBy, RequiredBy entry. e.g.

```
[Install]
WantedBy=local-fs.target
```

### Home

In home unit use

```
[Install]
RequiredBy=multi-user.target
```

Enable mount units
------------------

```sh
sudo systemctl enable tmp.mount
```
