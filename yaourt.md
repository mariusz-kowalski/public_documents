yaourt
======

```sh
sudo pacman --sync git
```

sudo nano /etc/makepkg.conf

```
# CFLAGS="-march=x86-64 -mtune=generic -O2 -pipe -fstack-protector-strong"
# CXXFLAGS="-march=x86-64 -mtune=generic -O2 -pipe -fstack-protector-strong"
CFLAGS="-march=native -O2 -pipe -fstack-protector-strong"
CXXFLAGS="${CFLAGS}"
```

```sh
sudo pacman --sync yajl
git clone https://aur.archlinux.org/package-query.git
cd package-query
makepkg
sudo pacman --upgrade package-query-*.pkg.tar.xz
cd ..
rm -rf package-query/
git clone https://aur.archlinux.org/yaourt.git
cd yaourt/
makepkg
sudo pacman --upgrade yaourt-*-any.pkg.tar.xz
cd ..
rm -rf yaourt/
```
