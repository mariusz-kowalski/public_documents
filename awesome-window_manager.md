Aewsome - window manager
========================

> @ToDo: add optional buut useful dependencies...

```sh
sudo pacman --sync aewsome
```

Hotkeys
-------

'$' is a mod key (win key)

* `$ + Shift + q` - **!!!** Kill X session, without **!!!**
* `$ + Shift + r` - Restart Awesome


* `$ + Enter` - Open a new terminal
* `$ + r`     - Run program, without any hint :(
* `$ + x`     - Run Lua code :(
* `$ + w`     - Open main menu

* `$ + Shift + c`      Close current window
* `$ + ←` / `$ + →`  Switch to left/right tag (tag is similar to desktop)
