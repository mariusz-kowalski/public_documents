Linux Performance Tools
=======================

> Work In Progress, tools are basically listed only, how-to's needed

Source materials
----------------

* [Linux Performance Tools, Brendan Gregg](https://www.youtube.com/watch?v=FJW8nGV4jxY)
* [Brendan Gregg's Homepage](http://www.brendangregg.com/index.html)

Workload Characterization Method
--------------------------------

1. **Who** is causing the load? PID, UID, IP addr, ...
2. **Why** is the load called? code path, stack trace
3. **What** is the load? IOPS, tput, type, r/w
4. **How** is the load changing over time?

The Use Method
--------------

* For every resource, check:
  1. **Utilization**
  2. **Saturation**
  3. **Errors**
* Definitions:
  * Utilization: busy time
  * Saturation: queue length or queued time
  * Errors: easy to interpreted
* Helps if you have a functional (block) diagram of your system / software /
  environment, showing all resources

Off-CPU Analysis
----------------

     |<-------preempted-or-time-quantum-expired--|  anon. major
     ↓                                           |  fault -->
    [RUNNABLE] --schedule-------------> [EXECUTING] <=========> [Anon. Paging]
     ↑                                           |\  <--page in
     |<--------wakeup-- [SLEEP] <------I/O wait--| \
     |<-------acquire-- [LOCK] <----------block--|  \
     |<--work-arrives-- [IDLE] <--wait-for-work--|   (On-CPU Profiling)

CPU Profile Methodology
-----------------------

1. Take a CPU profile (how?)
2. Understand all software in profile > 1%


* Discovers a wide range of performance issues by their CPU usage
* Narrows software to study

[Flame graph]

> How to generate flame graph?

```sh
git clone --depth 1 https://github.com/brendangregg/FlameGraph
cd FlameGraph
pref record -F 99 -a -g -- sleep 30
pref script | ./stackcollapse-pref.pl | ./flamegraph.pl > pref.svg
```

Observability Tools
-------------------

* Perform the USE Methodology for resource utilization (what resources?)
* Perform Workload Characterization for disks, networkk
* Perform the CPU Profile  Method using flame graphs
* Have exposure to various observability tools:
  * Basic: vmstat, iostat, mpstat, ps, top
  * Intermidiate: tcpdump, netstat, nicstat, pidstat, sar
  * Advanced: ss, slaptop, pref_events
* Perform Active Benchmarking
* Understand tuning risks
* Perform Static Performance Tuning

|                       Observability tools - block scheme                                                       |
|----------------------------------------------------------------------------------------------------------------|
|![Computer - block scheme](linux_performance_tools/linux_observability_tools.jpg)                               |
|License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) https://creativecommons.org/licenses/by-sa/4.0/|
|Credits: Brendan D. Gregg http://www.brendangregg.com/                                                          |
|Source: http://www.brendangregg.com/Perf/linux_observability_tools.png                                          |

### uptime

Shows CPUs + disks average load in periods of 1, 5, 15 min.
Load bigger than number of CPUs means CPU saturation - more things to compute
than available compute power. Things are getting queued.

### top (or htop)

%CPU is summed across all CPUs

### ps

### vmstat

Virtual memory statistics and more

```sh
vmstat -Sm 1
```

### iostat

```sh
iostat -xmdz 1
```

### mpstat

Multi-processor stats, per-CPU

```sh
mpstat -P ALL 1
```

### free

```sh
free -m
```

### strace

System call tracer

```sh
strace -tttT -p 313
```

> [**!**] Can slow the target by more than 100x.


### tcpdump

Sniff network packets for post analysis

```sh
tcpdump -i eth0 -w /tmp/out.tcpdump
```

### netstat

Various network protocol statistics using `-s`

```sh
netstat -s
```

### nicstat

Network interface stats, iostat-like output

```sh
nicstat 1
```

### pidstat

Very useful process stats. eg, by-thread, disk I/O

```sh
pidstat -t 1
pidstat -d 1
```

### swapon

Show swap device usage:

```
swapon -s
```

### lsof

Shows file descriptor usage, which for some apps, equals current active network
connections

```sh
lsof -iTCP -sTCP:ESTABLISHED
```

### sar

System Activity Reporter - many stats

```sh
sar -n TCP,ETCP,DEV 1
```

|sar - capabilities                                                                                              |
|----------------------------------------------------------------------------------------------------------------|
|![Computer - block scheme](linux_performance_tools/sar.jpg)                                                     |
|License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) https://creativecommons.org/licenses/by-sa/4.0/|
|Credits: Brendan D. Gregg http://www.brendangregg.com/                                                          |
|Source: http://www.brendangregg.com/Perf/linux_observability_sar.png                                            |

### ss

Socket statistics

```sh
ss -mop
ss -i
```

### iptraf

### iotop

Block device I/O (disk) by process:

```sh
iotop
```

> Needs kernel support enabled - CONFIG_TASKK_IO_ACCOUNTING

### slabtop

Kernel slab allocator memory usage

```sh
slabtop
```

### pcstat

Show page cache residency by file

```sh
./pcstat data0*
```

> useful for database performance analysis

### perf_events

* CPU profiling
* PMC profiling
* Static & dynamic tracing

### tiptop

IPC by process, %MISS, %BUS

### rdmsr

Model Specyfic Registers (MSRs), unlike PMCs, can be rad by default in Xen
guests

### other

* lrtace - library call tracer
* ethtool - interface tuning / stats
* snmpget - SNMP network host statistics
* lldptool - Can get LLDP broadcast stats
* blktrace - Block I/O event tracer
* /proc - Many raw kernel counters
* pmu-tools - On- and off- core CPU counter tools

Benchmarking Tools
------------------

* Multi:
  * UnixBench
  * lmbench
  * sysbench
  * pref bench
* FS/disk:
  * dd
  * hdparm
  * fio
* App/lib:
  * ab
  * wrk
  * jmeter
  * openssl
* Networking:
  * ping
  * hping3
  * ipref
  * ttcp
  * traceroute
  * mtr
  * pchar (measures bandwidth between hops)

| Linux benchmarking tools                                                                                       |
|----------------------------------------------------------------------------------------------------------------|
|![Computer - block scheme](linux_performance_tools/linux_benchmarking_tools.jpg)                                |
|License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) https://creativecommons.org/licenses/by-sa/4.0/|
|Credits: Brendan D. Gregg http://www.brendangregg.com/                                                          |
|Source: http://www.brendangregg.com/Perf/linux_benchmarking_tools.png                                           |

Tunning Tools
-------------

* Generic interfaces:
  * sysctl
  * /sys
* App
  * their own config
* CPU/scheduler:
  * nice
  * renice
  * taskset
  * ulimit
  * chcpu
* storage I/O
  * tune2fs
  * ionice
  * hdparm
  * blockdev
* Network
  * ethtool
  * tc
  * ip
  * route
* Dynamic patching:
  * stap
  * kpatch

Static tools
------------

Tuning and analysis of system without load

### CPU info

```
more /proc/cpuinfo
```

### CPU frequency scaling

```sh
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
```

> This is not Intel Turbo Boost (which is H/W)

### Storage devices

```sh
cat /proc/scsi/scsi
lsscsi
```

### Routing table

```sh
netstat -rn
ip route get 54.214.28.210
```

### other

* System messages:
  * `dmesg`
* Network interface config:
  * `ifconfig -a`
  * `ip link`
* file system capacity:
  * `df -h`
* Volume config:
  * `mdadm --misc -D /dev/md0`
* Storage device info:
  * `smartctl`
* NUMA config:
  * `numactl -s`
  * `numactl -H`
* PCI info:
  * `lspci`
* Kernel modules:
  * `lsmod`
* Root crontab config:
  * `crontab -l`
* Services:
  * `service --status-all`


Other
-----

* ./iosnoop - block I/O (disk) events with latency (blocktrece)
* ./opensnoop - what files are open?
* ./tpoint - who is creating disk I/O, and of what type?
* eBPF - in-kernel latency heat map
