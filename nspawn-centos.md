CentOS 6 doesn't work :-)

CentOS 7
========

How to create systemd-nspawn container with CentOS (RHEL probably too) on host
without native rpm/yum support, Arch in my case.

setup yum
---------

On yor host install yum. Make sure thet you have repos configured.
On arch you need to create file /etc/yum/repos.d/centos.repo

```
[centos]
name=centos
baseurl=https://ftp.fau.de/centos/7.2.1511/os/x86_64/
enabled=1
```

To check that yum works, run

```sh
sudo yum repolist
```

install guest OS
----------------

On BTRFS make subvolume for container

```sh
btrfs subvolume create _container_dir_
```

```sh
sudo yum groupinstall base  --skip-broken --nogpgcheck \
                            --installroot=/full/path/to/container/dir
```

setup container
---------------

To set root password, run container in chroot mode and 'passwd'

```sh
sudo systemd-nspawn --directory=wft_centos6
```

To setup network with systemd-networkd **boot** container with host network

```sh
sudo systemd-nspawn --boot --directory=_container_dir_ \
                    --machine=_container_name_
```

There install necessary packages

```sh
yum install openssh-server
yum install systemd-networkd systemd-resolved
```

Close container, hit Ctrl+] trhee times...

There ware lack of /usr/lib/systemd/network/80-container-host0.network file in
my case. In such case copy it from your host to your container.

```sh
sudo cp /usr/lib/systemd/network/80-container-host0.network \
        _container_dir_/usr/lib/systemd/network
```

working with container
----------------------

Boot container

```sh
sudo systemd-nspawn --boot --directory=_container_dir_ --network-veth \
                    --machine=_container_name_
```

Container should be available also via ssh through virutal network interface,
ip'ed with dhcp.

Ever changes made to the container will be permanent.

test-run of the container
-------------------------

It is required to place container on BTRFS filesystem to run this mode
(also LVM will do the job, I think).

To boot container into temporary mode add --ephemeral parameter.

```sh
sudo systemd-nspawn --boot --directory=_container_dir_ --network-veth \
                    --machine=_container_name_ --ephemeral
```

Every changes made to the container will be lost after power-down the
container. Container is run on btrfs snapshot of _container_dir_ and it is
thrown away after use.
