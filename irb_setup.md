IRB setup
=========

Completion, history and colored prompt.

~/.irbrc

```ruby
require 'irb/completion'
require 'irb/ext/save-history'
ARGV.concat [ "--readline", "--prompt-mode", "simple" ]
IRB.conf[:SAVE_HISTORY] = 1000
IRB.conf[:HISTORY_FILE] = "#{ENV['HOME']}/.irb-save-history"

IRB.conf[:PROMPT][:DEFAULT] = {
  :PROMPT_I => "\e[0;93m%N(%m):%03n:%i>\e[0m ",
  :PROMPT_S => "%N(%m):%03n:%i%l ",
  :PROMPT_C => "%N(%m):%03n:%i* ",
  :RETURN => "%s\n" # used to printf
}
```
