i3 setup
========

Example of my config (examples/i3/config).

install

```sh
sudo pacman --sync i3
```

Useful keybindings
------------------

```
# close window with middle button
bindsym --release button2 kill
```

i3blocks
--------

```sh
yaourt --sync i3blocks
sudo pacman --sync sysstat
mkdir .config/i3blocks
cp /etc/i3blocks.conf ~/.config/i3blocks/config
```

Edit in *~/.config/i3/config* file
```
bar {
        #status_command i3status
        status_command i3blocks
}
```

### pavucontrol button

Add to *~/.config/i3blocks/config* file
```
[vol]
interval=once
command=echo "VOL"; [[ -z "${BLOCK_BUTTON}" ]] || i3-msg exec pavucontrol
```

pavucontrol in floating mode
----------------------------

Add in *~/.config/i3/config* file
```
for_window [class="Pavucontrol"] floating enable, border normal
```

lock screen manually
--------------------

Add to *~/.config/i3/config* file
```
bindsym $mod+ctrl+l exec i3lock
```

Rofi
----

Rofi is cool - use it.

```sh
sudo pacman --sync rofi
```

### replace dmenu with rofi

```
bindsym $mod+d exec --no-startup-id "rofi -modi drun -show drun"
```

Commentout old dmenu_run or i3-dmenu-desktop entry

Add rofi window switcher

```
bindsym $mod+z exec --no-startup-id "rofi -modi window -show window"
```

Tips'n'tricks
-------------

Switch to window with specific title phrase or open some program. E.g. switch to
browser window with gmail or open new one if there is non.

```
bindsym $mod+Shift+g exec "i3-msg [title='gmail.com'] focus | grep -q ':false' && firefox --new-window https://gmail.com"
```