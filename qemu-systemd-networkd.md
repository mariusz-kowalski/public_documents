Qemu systemd-networkd
=====================

This document describes bridged networking for Qemu configured with
systemd-networkd.

> This solution is "under construction", I mean, I would like more somethig
  similar to systemd-nspawn networking, but for now I don't know how to do it.

Creatre bridge device - */etc/systemd/network/qemu0.netdev*

```
[NetDev]
Name = qemu0
Kind = bridge
```

Configure the bridge - */etc/systemd/network/qemu0.network*

```
[Match]
Name = qemu0

[Network]
Address=0.0.0.0/24
DHCPServer=yes
IPMasquerade=yes
```

Create qemu config dir */etc/qemu* (on my system there was no such dir, so I've
created it). Create file */etc/qemu/bridge.conf*

```
allow qemu0
```

Network options for `qemu-system-x86_64` command:

`-net nic,model=virtio -net bridge,br=qemu0`

Inside virtual machine start *systemd-networkd* and *systemd-resolved*
services and configure resolv.conf

```sh
rm /etc/resolv.conf
ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
```
