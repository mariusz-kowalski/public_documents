Archlinux - the installation
============================

 <!-- @FixMe:
	* PARTLABEL for partition for OS root filesystem should be named "sys0",
	  this is partition for operating systems (possibly multiple) and other
	  closely related stuff e.g. "/tmp"
	* subvolume for OS root filesystem should be named "arch0", and its snapshots
	  "arch0-2017.04.22-20.44"
	  OR
	  each system should have its subvolume and snapshots in a dir e.g.
	  "os/arch0/2017.04.22-20.44" <- subvol
	  "os/arch0/2017.04.28-20.44" <- snapshot
	  that way, `ls os` gives you a list of OSes
	* sys0 partition should be mounted as partition (not subvolume) on
	  /mnt/partition/sys0

 -->

Assumptions
-----------

* EFI boot system
* GPT disk format
* BTRFS filesystem (except efi and swap partition)
* separate partition for OS (/root) and data (/home /var etc)
* spare (rescue) OS (separate partition)
* bootable OS snapshots
* boot manager: systemd-boot
* network: systemd-networkd, systemd-resolvd, wpa_supplicant

As file system I've chosen BTRFS instead of LVM because BTRFS is cool, new and
shinny ;-) It has data compression on the fly, COW, nice subvolume
system and few features on the way.

> My disk is `/dev/sda`, yours might be different.

Partitions
----------

To check current status of what is going on the disks run

```sh
lsblk --output MODEL,NAME,SIZE,LABEL,TYPE,FSTYPE,UUID,PARTUUID,PARTLABEL,MOUNTPOINT
```

To remove all partitions on disk

```sh
wipefs /dev/sda # this will display the offset
wipefs --offset <offset> /dev/sda
```

Hmm... gdisk will repair GPT form backup (backup of GPT is somewhere on the disk)

If you need erase all partitions use gdisk, command `o`. Exit gdisk and write
changes, to be sure.

Open gdisk (Don't use parted - this program is crazy)

```sh
gdisk /dev/sda
```

### EFI Partition

* partition number: 1
* size: 512M
* type: EF00

Gdisk will mark this partition as boot partition.

### SWAP

* partition number: 2
* size: same as RAM
* type: 8200
* label: "Swap"


To set label use command *c*.

### Root

* partition number: 3
* size:
  * at least 8G size for server
  * 16G for desktop
* type 8300
* label: "OS"

### Spare operating system

* partition number: 4
* size: 6G
* type: 8300
* label: "SpareOS"

Spare operating system can be very helpful when you need to backup your main
one. And it's indispensable when your main system is broken so badly that it
can't boot. You can also experiment on it.

### Data storage

* partition number: 5
* size: rest of the free space
* type: 8300
* label: *Storage*

File systems
------------

### EFI

```sh
mkfs.fat -F32 /dev/sda1
```

### Swap

```sh
mkswap /dev/sda2
swapon /dev/sda2
```

### Root

Set label, it's helpful when listing filesystems, but
never use label as id when mounting. Label can be changed and it causes
disaster. Trust me I've tried.

```sh
mkfs.btrfs --label root /dev/sda3
```

Use `--force` option if you need to replace old fs.

### Spare root

```sh
mkfs.ext4 -L spare_root /dev/sda4
```

### Storage

```sh
mkfs.btrfs --label storage /dev/sda5
```

setup root and boot file system
-------------------------------

Install system on subvolume on "root" partition.
There is also subvolume "tmp" on "root" partition and "var" subvolume on
"storage" partition. "tmp" and "var" subvolumes have disabled copy-on-write
feature. On "storage" partition is also "home" and "machines" subvolume.

> `ssd` and `discard` options are only for SSD drives.

```sh
mkdir /mnt/sda3
mount /dev/sda3 /mnt/sda3 -o strictatime,lazytime,compress=lzo,space_cache,ssd,discard
cd /mnt/sda3
btrfs subvolume create root
btrfs subvolume create tmp
chattr +C tmp
cd /
mkdir /mnt/sda5
mount /dev/sda5 /mnt/sda5 -o strictatime,lazytime,compress=lzo,space_cache,ssd,discard
cd /mnt/sda5
btrfs subvolume create var
chattr +C var
btrfs subvolume create home
```

Mount system filesystems.

> Use `nodatacow` for "var" and "tmp"

```sh
cd /
mkdir /mnt/root
mount /dev/sda3 /mnt/root -o subvol=root,strictatime,lazytime,compress=lzo,space_cache,ssd,discard
mkdir /mnt/root/boot
mount /dev/sda1 /mnt/root/boot
mkdir /mnt/root/tmp
mount /dev/sda3 /mnt/root/tmp -o subvol=tmp,strictatime,lazytime,compress=lzo,space_cache,ssd,discard,nodatacow
mkdir /mnt/root/var
mount /dev/sda5 /mnt/root/var -o subvol=var,strictatime,lazytime,compress=lzo,space_cache,ssd,discard,nodatacow
mkdir /mnt/root/home
mount /dev/sda5 /mnt/root/home -o subvol=home,strictatime,lazytime,compress=lzo,space_cache,ssd,discard
```

Install the base packages
-------------------------

```sh
pacstrap -i /mnt/root base base-devel bash-completion
```

Prepare fstab
-------------

Generate fstab file. Make sure that root entry has these options
*strictatime,lazytime,compress=lzo,ssd,space_cache,discard* and *nodatacow* for
tmp and var.

> genfstab generate invalid fstab file for subvolumes!

```sh
genfstab -U /mnt/root > /mnt/root/etc/fstab
```

Replace UUID with PARTLABEL. Drop subvloid.
Manually add:
* btrfs partitions (raw, not subvolums).
* *machines* subvolume

Example:

```
PARTLABEL=OS		/         	btrfs     	rw,lazytime,compress=lzo,ssd,discard,space_cache,subvol=/root	0 0

# /dev/sda1
UUID=3FEE-FBBF      	/boot     	vfat      	rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro	0 2

PARTLABEL=OS		/tmp      	btrfs     	rw,lazytime,compress=lzo,ssd,discard,space_cache,subvol=/tmp,nodatacow	0 0
PARTLABEL=Storage	/var      	btrfs     	rw,lazytime,compress=lzo,ssd,discard,space_cache,subvol=/var,nodatacow	0 0
PARTLABEL=Storage	/home     	btrfs     	rw,lazytime,compress=lzo,ssd,discard,space_cache,subvol=/home	0 0

PARTLABEL=Swap		none      	swap      	defaults  	0 0

# partitions
PARTLABEL=OS		/mnt/partition/OS	btrfs	rw,lazytime,compress=lzo,ssd,discard,space_cache	0 0
PARTLABEL=Storage	/mnt/partition/Storage	btrfs	rw,lazytime,compress=lzo,ssd,discard,space_cache	0 0
```

make partition dirs

```sh
mkdir -p /mnt/partition/OS
mkdir -p /mnt/partition/Storage
```

### machines

If you use subvolume on different partition (*Storage* in my case) remove
*/var/lib/machines* subvolume, make that dir, and make entry in fstab for it.

```sh
btrfs subvolume delete /var/lib/machines
mkdir /var/lib/machines
```

```/mnt/root/etc/fstab
# ...
# machines
PARTLABEL=Storage       /var/lib/machines       btrfs   rw,lazytime,compress=lzo,ssd,discard,space_cache,subvol=/machines   0 0
# ...
```

Chroot into new os
------------------

```sh
arch-chroot /mnt/root /bin/bash
```

Locale, in file */etc/locale.gen* uncomment these entries

```sh
cat << EOF > /etc/locale.gen
en_US.UTF-8 UTF-8
pl_PL.UTF-8 UTF-8
EOF
```

then

```sh
locale-gen
```

create /etc/locale.conf

```
cat << EOF > /etc/locale.conf
LANG=en_US.UTF-8
LANGUAGE=en_US:en
LC_CTYPE=pl_PL.UTF-8
LC_NUMERIC=pl_PL.UTF-8
LC_TIME=pl_PL.UTF-8
LC_COLLATE=pl_PL.UTF-8
LC_MONETARY=pl_PL.UTF-8
LC_PAPER=pl_PL.UTF-8
LC_MEASUREMENT=pl_PL.UTF-8
EOF
```

Configure console. In file */etc/vconsole.conf*

```
cat << EOF > /etc/vconsole.conf
KEYMAP=pl2
FONT=ter-228n
FONT_MAP=8859-2
EOF
```

Install font

```sh
pacman --sync terminus-font
```

Set timezone.

```sh
rm /etc/localtime
ln -s /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
```

Configure initramfs, in file */etc/mkinitcpio.conf* set this options (module
i915 is for Intel graphics card)

```
MODULES="i915"
HOOKS="base udev autodetect modconf block filesystems btrfs keyboard fsck"
```

Install btrfs-progs, without it boot will fail.

```sh
pacman --sync btrfs-progs
```

Regenerate initramfs image.

```sh
mkinitcpio -p linux
```

Setup bootloader
----------------

### systemd-boot

I'm using systend-boot because it is extremely simple.

```sh
bootctl install
```

Configure bootloader, */boot/loader/loader.conf*

```
timeout 0
default root
editor  0
```

Create bootloader entry file */boot/loader/entries/root.conf*

```
title	Root - Arch Linux
linux	/vmlinuz-linux
initrd	/intel-ucode.img
initrd	/initramfs-linux.img
options	root=PARTLABEL=OS rw rootflags=subvol=root
```

```sh
pacman --sync intel-ucode
```

Add pacman hook to update bootloader after update of the systemd.

*/etc/pacman.d/hooks/systemd-boot.hook*

```
[Trigger]
Type = Package
Operation = Upgrade
Target = systemd

[Action]
Description = Updating systemd-boot
When = PostTransaction
Exec = /usr/bin/bootctl update
```

### syslinux

<!-- @ToDo: move this section to alternative installation method -->

> Use intel-ucode only if you have Intel CPU

```sh
pacman --sync syslinux gptfdisk intel-ucode
```

Setup the bootloader.

```sh
syslinux-install_update -i -a -m
```

Configure the bootloader. */boot/syslinux/syslinux.cfg*

```
DEFAULT arch
PROMPT 0        # Set to 1 if you always want to display the boot: prompt
TIMEOUT 50

LABEL arch
    LINUX ../vmlinuz-linux
    APPEND root=/dev/sdb1 rw    # <-- your root partition
    INITRD ../intel-ucode.img,../initramfs-linux.img

LABEL archfallback
    LINUX ../vmlinuz-linux
    APPEND root=/dev/sdb1 rw    # <-- your root partition
    INITRD ../intel-ucode.img,../initramfs-linux-fallback.img
```

Hostname
--------

Set machine name in file /etc/hostname.

Set root password
-----------------

```sh
passwd
```

Wifi
----

You can configure wifi later but if you don't have wired connection install all
dependencies now.

```sh
pacman --sync wpa_supplicant
```

### Quick example of configuration

* wifi interface *wlp2s0*

Configure network

*/etc/systemd/network/wireless.network*

```
[Match]
Name=wlp2s0

[Network]
DHCP=ipv4
```

```sh
systemctl enable systemd-networkd
systemctl enable systemd-resolved
```

*/etc/wpa_supplicant/wpa_supplicant-wlp2s0.conf*

```
ctrl_interface=/run/wpa_supplicant
update_config=1

network={
	ssid="your-wifi-ssid"
	psk="your-wifi-password"
}
```

```sh
systemctl enable wpa_supplicant@wlp2s0
```

Read more on:

[wifi - wpa_supplicant wpa_gui systemd-networkd](wifi-wpa_supplicant-wpa_gui-systemd-networkd)

Exit
----

```sh
exit
umount --recursive /mnt/root
umount /mnt/sda3
umount /mnt/sda5
reboot
```

Checkout "Archlinux, after installation"

<!--  LocalWords:  subvolume subvolumes filesystem
 -->
