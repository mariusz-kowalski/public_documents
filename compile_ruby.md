Compile Ruby
============

Version 2.2.3, it requires patch for new version of openssl.

Dependencies
------------

```bash
sudo pacman --sync --needed base-devel
```

Optionally

```bash
sudo pacman --sync --needed wget
```

Download source
---------------

```bash
wget wget https://cache.ruby-lang.org/pub/ruby/2.2/ruby-2.2.3.tar.gz
tar zxf ruby-2.2.3.tar.gz
cd ruby-2.2.3
```

Patch source
------------

> THIS step is only for ruby version 2.2.3. Skip it on any different version.

Create a patch file openssl.patch

```diff
diff --git a/ext/openssl/extconf.rb b/ext/openssl/extconf.rb
index 8c04cb5..132d803 100644
--- a/ext/openssl/extconf.rb
+++ b/ext/openssl/extconf.rb
@@ -104,6 +104,9 @@
 have_func("SSLv2_method")
 have_func("SSLv2_server_method")
 have_func("SSLv2_client_method")
+have_func("SSLv3_method")
+have_func("SSLv3_server_method")
+have_func("SSLv3_client_method")
 have_func("TLSv1_1_method")
 have_func("TLSv1_1_server_method")
 have_func("TLSv1_1_client_method")
diff --git a/ext/openssl/ossl_ssl.c b/ext/openssl/ossl_ssl.c
index f7cb7f0..47111f6 100644
--- a/ext/openssl/ossl_ssl.c
+++ b/ext/openssl/ossl_ssl.c
@@ -109,9 +109,12 @@ static const struct {
     OSSL_SSL_METHOD_ENTRY(SSLv2_server),
     OSSL_SSL_METHOD_ENTRY(SSLv2_client),
 #endif
+#if defined(HAVE_SSLV3_METHOD) && defined(HAVE_SSLV3_SERVER_METHOD) && \
+        defined(HAVE_SSLV3_CLIENT_METHOD)
     OSSL_SSL_METHOD_ENTRY(SSLv3),
     OSSL_SSL_METHOD_ENTRY(SSLv3_server),
     OSSL_SSL_METHOD_ENTRY(SSLv3_client),
+#endif
     OSSL_SSL_METHOD_ENTRY(SSLv23),
     OSSL_SSL_METHOD_ENTRY(SSLv23_server),
     OSSL_SSL_METHOD_ENTRY(SSLv23_client),
```

```bash
patch -p1 < openssl.patch
```

Optimize compiler
-----------------

Edit options in file /etc/makepkg.conf

```
CFLAGS="-march=native -O2 -pipe -fstack-protector-strong"
CXXFLAGS="${CFLAGS}"

MAKEFLAGS="-j4"
```

configure, compile, install
---------------------------

```bash
./configure
make
sudo make install
```

User env configure
------------------
Install gems (also via bundler) in home directory.

~/.bashrc

```bash
export GEM_HOME=$(ruby -rubygems -e "puts Gem.user_dir")
export PATH=$GEM_HOME/bin:$PATH
```

Don't install documentation for gems.

~/.gemrc

```bash
gem: --no-ri --no-rdoc --no-document
```
