LXQt
====

```sh
pacman --sync --needed lxqt oxygen-icons libpulse libsysstat \
      lm_sensorslxappearance-gtk3 lxappearance-obconf-gtk3

```

run with xinitrc, add to *~/.xinitrc*

```
exec startlxqt
```

Bad news everyone - LXQt is buggy (at the tieme of writing this - 2016-12-29).
Brightness changes brightness of external monitor instead of laptop panel, and
there are only round widgets without specific information about brightness.

Monitor settings doesn't work at all.
