Proxy
=====

If you need proxy configuration, create global proxy file like this:
/etc/profile.d/proxy.sh

```bash
proxy=http://127.0.0.1:8888/

export http_proxy=$proxy
export https_proxy=$proxy
export ftp_proxy=$proxy

export HTTP_PROXY=$proxy
export HTTPS_PROXY=$proxy
export FTP_PROXY=$proxy

export no_proxy=localhost,127.0.0.0/8,*.local
export NO_PROXY=$no_proxy
```
