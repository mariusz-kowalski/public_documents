Archlinux, after installation
=============================

Mute pc speaker
---------------

```sh
rmmod pcspkr
echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf
```

Keep boot massages
------------------

```sh
mkdir /etc/systemd/system/getty@tty1.service.d/
```

Create file

/etc/systemd/system/getty@tty1.service.d/noclear.conf

```sh
[Service]
TTYVTDisallocate=no
```

Network
-------

```sh
systemctl enable systemd-networkd
systemctl enable systemd-resolved
systemctl start systemd-networkd
systemctl start systemd-resolved

rm /etc/resolv.conf
ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
```

If you have wired network, create file */etc/systemd/network/wired.network*

```sh
[Match]
Name=enp0s25

[Network]
DHCP=ipv4
```

To apply new settings run

```sh
systemctl restart systemd-networkd
```

@TODO: more about network

Necessary tools
---------------

```sh
sudo pacman --sync bash-completion
```

```sh
sudo pacman --sync openssh
```

To allow root login in file */etc/ssh/sshd_config* set

> This is security risk! Do this if you know what you are doing.

```sh
PermitRootLogin yes
```

```sh
systemctl enable sshd
systemctl start sshd
```

Make wraper on lsblk
--------------------

Create script */usr/local/bin/lsblk_ext*

```sh
#!/bin/sh
lsblk --output NAME,LABEL,MODEL,SIZE,TYPE,FSTYPE,MOUNTPOINT,UUID,PARTUUID
```

Make it executable.

```sh
chmod a+x /usr/local/bin/lsblk_ext
```

/home file system
-----------------

Create filesystem on partition dedicated to home.

```sh
mkfs.btrfs --label home /dev/sda6
```

Check UUID of partition with our own created script.

```sh
lsblk_ext
```

Add entry to */etc/fstab*.

> options **ssd** and **discard** only for SSD.

```sh
UUID=fb131d42-b375-t811-t94f-d64tt69f3c97	/home		btrfs		rw,noatime,compress=lzo,ssd,space_cache,discard		0	0
```

Mount it.

```sh
mount /home
```

nano
----

My config.

```sh
sudo wget --output-document=/etc/nanorc \
     https://gist.githubusercontent.com/mariusz-kowalski/c2cc0ef14be41b51f0ee/raw/b12dc575d26adb91e8adc5cba209fd9c9ea06772/nanorc
```

Add user
--------

```sh
useradd --create-home --groups wheel --shell /bin/bash _some_user_
passwd _some_user_
```

### sudo

Edit /etc/sudoers with `sudoedit`

```sh
export EDITOR=nano # if you want use nano instead of vim
sudoedit /etc/sudoers
```

```
%wheel ALL=(ALL) ALL
```

Network time
------------

```sh
systemctl enable systemd-timesyncd
systemctl start systemd-timesyncd
```

Desktop
-------

If you prefer classic window manager go for XFCE4. Do not use Gnome 3 it's
crippled version of good old Gnome 2 with fancy but useless visual effects.
Don't be misleaded by good looking design of new KDE5, it looks really good.
I even suspect that some graphics designer works for KDE. Unfortunately KDE
established new tradition of rewriting everything from scratch, so yes,
KDE is unstable again, it's slow again and, yes, it's uncompatible AGAIN.
You can wait few years, all those problems will be solved, then you will be
"surprised" with KDE6.

XFCE on the other hand is develop in Unix spirit. It's light and fast even on
low performance machines. One program do one thing and do it right. XFCE is
developed maybe slowly but steady and the most important without breaking
compatibility.

You should try tiling window manager *i3*. It's most user friendly of all tiling
window managers known to mankind. That doesn't mean it is very user friendly, in
fact it's not. It means others are worse.

Worth mention is Fluxbox - super ultra light window manager. You can try it if
you have low memory or you just don't use GUI that much.

```sh
sudo pacman --sync --needed xorg-server xorg-server-utils xorg-apps xorg-xinit \
  xf86-video-intel libva-intel-driver mesa-vdpau libva-mesa-driver \
  libvdpau-va-gl xterm xorg-twm
```

and fonts:

```sh
sudo pacman --sync --needed noto-fonts noto-fonts-emoji ttf-bitstream-vera \
  ttf-croscore ttf-dejavu ttf-freefont ttf-linux-libertine ttf-droid \
  ttf-liberation ttf-ubuntu-font-family
```

create X config file for Intel graphics card
*/etc/X11/xorg.conf.d/20-intel.conf*

```
Section "Device"
	Identifier	"Intel Graphics"
	Driver		"intel"
	#Option		"AccelMethod"	"uxa" # old method - try it if sna fails
	Option		"AccelMethod"	"sna"
EndSection
```

@TODO: more options...

X config
--------

Set global DPI manually. Edit */etc/X11/xinit/xserverrc*
```
exec /usr/bin/X -nolisten tcp "$@" -dpi 144
```

Good values to choose 96, 120 (25% higher), 144 (50% higher), 168 (75% higher),
192 (100% higher) etc.

Xfce4
-----

```sh
sudo pacman --sync xfce4 xfce4-goodies
```

XDG
---

```sh
sudo pacman --sync xdg-user-dirs
xdg-user-dirs-update
```

@TODO: Figure out where come from all the XDG_ variables...

Sound
-----

Install pulse audio and volume controller. People says bad thing about it, but
pulse audio do it's job.

```sh
sudo pacman --sync pulseaudio pavucontrol pulseaudio-alsa
```

Pulseaudio will start automatically after next reboot and after installation you
can run it manually

```sh
pulseaudio --start
```

@TODO: volume control applet for xfce.

Wifi
----

[wpa_supplicant wpa_gui systemd-networkd](/wpa_supplicant-wpa_gui-systemd-networkd.md)

more tools
----------

* (/zsh.md)
* (/yaourt.md)
* (/lilyterm.md)
