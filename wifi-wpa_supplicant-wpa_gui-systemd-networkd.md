wifi - wpa_supplicant wpa_gui systemd-networkd
==============================================

> under construction :)

```sh
sudo pacman --sync wpa_supplicant wpa_supplicant_gui
```

In this example **wlp2s0** is wifi interface.


Network configuration
---------------------

Create file */etc/systemd/network/wireless.network*

```
[Match]
Name = wlp2s0

[Network]
DHCP = ipv4

[DHCP]
RouteMetric = 20
```

Set *MetricRoute* 10 for wired network

Wpa_supplicant configuration
----------------------------

Create file */etc/wpa_supplicant/wpa_supplicant-wlp2s0.conf*

```
ctrl_interface=/run/wpa_supplicant
update_config=1
```

```sh
systemctl enable wpa_supplicant@wlp2s0
systemctl start wpa_supplicant@wlp2s0
```

Connect to WiFi via GUI
-----------------------

Run `wpa_gui` configure your connection and save it as
*/etc/wpa_supplicant/wpa_supplicant-wlp2s0.conf*

Connect to WiFi via CLI
-----------------------

run `wpa_cli`

```
interface wlp2s0
scan
scan_results
add_network
set_network 0 ssid "MYSSID"
set_network 0 psk "passphrase"
enable_network 0
save_config
quit
```
