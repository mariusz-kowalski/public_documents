Archlinux - btrfs - boot snapshot
=================================

Assumptions:

* OS is installed according this (archlinux-the-installation.md)

Make a snapshot
---------------

```sh
sudo btrfs subvolume snapshot /mnt/partition/OS/root /mnt/partition/OS/root-$(date +%Y.%m.%d-%H.%M)
```

Amend fstab in the snapshot, */mnt/partition/OS/root-2017.04.22-20.44/etc/fstab*
Modify subvol of "/"

```
PARTLABEL=OS  /  btrfs  rw,lazytime,compress=lzo,ssd,discard,space_cache,subvol=/root-2017.04.22-20.44	0 0
```

Create boot loader entry.

```sh
cd /boot/loader/entries/
cp root.conf root-2017.04.22-20.44.conf
```

Modify subvol parameter *root-2017.04.22-20.44.conf*

```
title	Root - Arch Linux - 2017.04.22-20.44
linux	/vmlinuz-linux
initrd	/intel-ucode.img
initrd	/initramfs-linux.img
options	root=PARTLABEL=OS rw rootflags=subvol=root-2017.04.22-20.44
```

> THIS WILL FAIL AFTER KERNEL UPDATE !!! WORK IN PROGRESS

Boot loader entry should contain specific kernel version, that kernel version
should be saved (the kernel and the modules).

Keeping old kernels
-------------------

Keep current vmlinux-linux and initramfs-linux.img

> Don't do it if you upgraded the kernel but not rebooted the computer yet!

```sh
sudo cp /boot/vmlinuz-linux /boot/vmlinuz-linux-$(uname --kernel-release)
sudo cp /boot/initramfs-linux.img /boot/initramfs-linux-$(uname --kernel-release).img

cat << EOF | sudo tee /boot/loader/entries/$(uname --kernel-release)-root.conf
title $(uname --kernel-release) - root
linux /vmlinuz-linux-$(uname --kernel-release)
initrd /intel-ucode.img
initrd /initramfs-linux-$(uname --kernel-release).img
options root=PARTLABEL=OS rw rootflags=subvol=root
EOF
```

Make it boot by deafult

```sh
cat << EOF | sudo tee /boot/loader/loader.conf
timeout 10
default $(uname --kernel-release)-root
editor 0
EOF
```

Keep kernel modules... :/ Howww???

```sh
sudo mkdir -p /usr/lib/modules/archives
sudo cp --reflink=auto --recursive /usr/lib/modules/$(uname --kernel-release) /usr/lib/modules/archives
kprefix=$(uname --kernel-release | cut --delimiter="." --fields=1,2)
ksuffix=$(uname --kernel-release | cut --delimiter="-" --fields="2-3" | cut --delimiter="-" --fields=2)
sudo cp --reflink=auto --recursive /usr/lib/modules/extramodules-${kprefix}-${ksuffix} /usr/lib/modules/archives
```

> NOT TESTED !!! NOT EVEN ONCE

Restore a snapshot
------------------

When you made some changes on your system (HEAD) and you want to restore it to
some backup snapshot.

Root partition should be polarized, that means it should be named *root-n* or
*root-s*. If your root partition is not polarized yet, polarize it now.

Create new (polarized) root snapshot form some backup. If you have polarized
root - create other pole, if not just create any pole.

# N pole

```sh
sudo btrfs subvolume snapshot /mnt/partition/OS/root-2017.04.28-01.46 /mnt/partition/OS/root-n
```

Update fstab on new snapshot

*/mnt/partition/OS/root-n/etc/fstab*

```
PARTLABEL=OS            /               btrfs     rw,lazytime,compress=lzo,ssd,discard,space_cache,subvol=/root-n   0 0
```

Create boot loader entry. Load kernel version that is suitable for the snapshot.
Check */mnt/partition/OS/root-2017.04.28-01.46/usr/lib/modules* for modules
versions

```sh
kernel_release=4.10.11-1-ARCH
pole=n
cat << EOF | sudo tee /boot/loader/entries/${kernel_release}-root-${pole}.conf
title $(uname --kernel-release) - root-${pole}
linux /vmlinuz-linux-${kernel_release}
initrd /intel-ucode.img
initrd /initramfs-linux-${kernel_release}.img
options root=PARTLABEL=OS rw rootflags=subvol=root-${pole}
EOF
```

Make it boot by deafult

```sh
kernel_release=4.10.11-1-ARCH
pole=n
cat << EOF | sudo tee /boot/loader/loader.conf
timeout 10
default ${kernel_release}-root-${pole}
editor 0
EOF
```
