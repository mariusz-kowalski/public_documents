ZSH
===

```sh
sudo pacman --sync zsh zsh-completions zsh-syntax-highlighting

echo "source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" \
     >> /etc/zsh/zshrc

yaourt --sync oh-my-zsh-git
echo "source /usr/share/oh-my-zsh/zshrc" >> /etc/zsh/zshrc
```

Install my theme

```sh
cd /usr/local/share/
git clone https://github.com/mariusz-kowalski/devpad_prompt.git
echo "source /usr/local/share/devpad_prompt/devpad_prompt.zsh-theme" \
    >> /etc/zsh/zshrc
```

Each user must execute thid to switch default shell to Zsh

```sh
echo '' > ~/.zshrc
chsh --shell /bin/zsh
```

> ToDo: default configuration?
