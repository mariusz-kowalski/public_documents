Laptop / Tablet Sony Vaio Multiflip 13
======================================

Touchpad
--------

```sh
sudo pacman --sync xf86-input-libinput
sudo pacman --remove --recursive xf86-input-evdev
```

*/etc/X11/xorg.conf.d/30-touchpad.conf*
```W
Section "InputClass"
    Identifier "devname"
    Driver "libinput"
    #Option "Device"   "devpath"
    Option "Tapping" "on"
EndSection
```

Backlight
---------

```sh
yaourt --sync acpilight
```

Allow wheel group members to run `sudo xbacklight` without password, create file
*/etc/sudoers.d/xbacklight*

```
%wheel ALL= NOPASSWD: /usr/bin/xbacklight
```

Key bindings
------------

### X

*~/.xbindkeysrc*
```
"pamixer --increase 5"
    XF86AudioRaiseVolume

"pamixer --allow-boost --increase 5"
    Control + XF86AudioRaiseVolume

"pamixer --decrease 5"
    XF86AudioLowerVolume

"pamixer --allow-boost --decrease 5"
    Control + XF86AudioLowerVolume

"pamixer --toggle-mute"
     XF86AudioMute

# backlight
"sudo xbacklight -inc 5"
    XF86MonBrightnessUp

"sudo xbacklight -dec 5"
    XF86MonBrightnessDown
```

Keyboard backlight
------------------

*/etc/modprobe.d/sony-keyboard-backlight.conf*

```
options sony_laptop kbd_backlight_timeout=60 kbd_backlight=1
```

Power managment
---------------

### battery

```sh
sudo pacman --sync acpi
```

### X

Create file */etc/X11/xorg.conf.d/10-monitor.conf*
```
Section "Monitor"
  Identifier  "eDP1"
  Option      "DPMS" "true"
EndSection

Section "ServerLayout"
  Identifier  "ServerLayout0"
  Option      "StandbyTime" "10"
EndSection
```

Make standby lock the screen, create file
*/etc/X11/xinit/xinitrc.d/70-screen-locker.sh* and make it executeble.
```
xss-lock -- i3lock -n &
```

### Console

Create */etc/profile.d/blank.sh* file and make it executable.
```
setterm --blank 10
```

|                  Caution                  |
|:-----------------------------------------:|
|This procedure **does not** lock the screen|
