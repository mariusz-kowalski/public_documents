---
layout: post
title:  "JavaScript quick tips - objects"
date:   2016-01-08 18:10:00
---

## Objects

What is object? Code is worth thousands words...

{% highlight JavaScript %}
var user = {
  name: 'Jack',
  present_yourself: function(){
    console.log("My name is " + this.name)
  },
  'address': "USA"
}

user.present_yourself()
console.log(user.name)
console.log(user['name'])
user.name = "John"
user['name'] = "Johny"
user['present_yourself']() // don't do that way ;)

user.age = 32
console.log(user.age)

user.present_yourself = function(){
  console.log("My name is " + this.name + ", I'm " + this.age)
}
user.present_yourself()
```

This is literal object notation.

## Constructors

Objects can have constructor. Actually ever object have one, but in literal
notation default one is used with no parameters.

{% highlight JavaScript %}
function Person(name){
  this.name = name
  this.present_yourself = function(){
    console.log("My name is " + this.name)
  }
}
joe = new Person("Joe")
joe.present_yourself()

var mike = new Person("Mike")
mike.name = "Mikkee"
mike.present_yourself()
```

Name of constructor with a capital letter is a convention. Objects constructor
is an objects method **constructor**.

{% highlight JavaScript %}
function Person(name){
  this.name = name
  this.present_yourself = function(){
    console.log("My name is " + this.name)
  }
}
joe = new Person("Joe")

var mike = new joe.constructor("Mike")
mike.present_yourself()
```

## Object origin

{% highlight JavaScript %}
function Person(name){
  this.name = name
}
var joe = new Person("Joe")

var mic = {
  name: "Michael"
}

console.log(joe instanceof Person)
console.log(joe instanceof Object)
console.log(mic instanceof Object)
// -> true
//    true
//    true
```

Every object inherits from Object. Object has **constructor** property which
can be used to create similar object - like same class in normal languages but
not exactly. Check this out

{% highlight JavaScript %}
function Person(name){
  this.name = name
  this.present_yourself = function(){
    console.log("My name is " + this.name)
  }
}
joe = new Person("Joe")

// create new person like joe
var mike = new joe.constructor("Mike")

// change joe
joe.present_yourself = function(){
  console.log("I'm " + this.name)
}

// mike is still like old joe
mike.present_yourself()

// maybe if we create new person from new joe
var klark = new joe.constructor("Klark")
klark.present_yourself()
// nope, we've changed only one method of joe
// joes  constructor still constructs people like old joe

// so lets change joes constructor
joe.constructor = function(name){
  this.name = name
  this.present_yourself = function(){
    console.log("I'm " + this.name + ". What's your name?")
  }
}

// create new person like joe
var ellie = new joe.constructor("Ellie")

joe.present_yourself() // joe has only second change
mike.present_yourself() // mike didn't change
klark.present_yourself() // same klark
ellie.present_yourself() // ellie is totally differed person than joe...
```

Object has **toString()** method which is used in many situations like casting
to string

{% highlight JavaScript %}
function Person(name){
  this.name = name
  this.toString = function(){
    return "mighty " + name
  }
}
var joe = new Person("Joe")
console.log("I'm " + joe)
// -> I'm mighty Joe
```

Object has **valueOf()** method which basically returns self. I don't know for
what is this.

## Array is an object.

{% highlight JavaScript %}
var a = [1, 2, 3]
console.log(a instanceof Object)
// -> true
```

## Function is an object.

{% highlight JavaScript %}
function print(message) {
  console.log(" >>> " + message)
}
print("one two three")

console.log(print instanceof Object)
console.log(print.toString())

print.color = "yellow"
console.log(print.color)


print.loud = function(message){
  console.log(" >>> " + message + "!!!")
}

print.loud("hello world")
print("What's up?")
```

### function source

{% highlight JavaScript %}
function print(message) {
  console.log(" >>> " + message)
}
console.log(print.toString())
// -> function print(message) {
//      console.log(" >>> " + message)
//    }
```

### call method on different object

{% highlight JavaScript %}
var person = {
  goodbye: "Bye!",
  say_goodbye: function(){
    console.log(this.goodbye)
  }
}
person.say_goodbye()

var arnie = {
  goodbye: "Hasta la vista, baby!"
}

person.say_goodbye.call(arnie)

// -> Bye!
//    Hasta la vista, baby!
```

### Curiosities

{% highlight JavaScript %}
function luke(){
  alert('use your source')
}
luke.toString()

// -> function luke(){
//      alert('use your source')
//    }
```

Variable *arguments* is not only list of argument. It contains reference to
the function.

{% highlight JavaScript %}
(
  function(count){
    if(count < 3){
      console.info("I can call anonymous function " + count)
      arguments.callee(count + 1)
    }
  }
)(0)

// -> I can call anonymous function 0
//    I can call anonymous function 1
//    I can call anonymous function 2
```
