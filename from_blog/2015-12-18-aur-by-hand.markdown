---
layout: post
title:  "AUR by hand"
date:   2015-12-18 07:58:00
no_comments: false
---

## create place

As root.

```sh
cd /var/
btrfs subvolume create aur # this will exclude it from root snapshoot
chattr +C aur # this will disable copy-on-write
groupadd aur    #
chown :aur aur  # group for users
chmod +s aur    # that can write here
chmod g+w aur/  #
```

Add some user to group aur.

<pre>
gpasswd -a _some_user_ aur
</pre>

It will work after next login or in terminal you can do

<pre>
su - _some_user_
</pre>

## install sample aur, gitflow in this example

As user in *aur* group.

<pre>
cd /var/aur
git clone https://aur.archlinux.org/gitflow-git.git
cd gitflow-git
</pre>

## Get dependencies

This will display make and package dependencies. You can remove makedeps after
package is built.

<pre>
grep dep PKGBUILD
</pre>

Install them with pacman with option
<pre>
--needed
</pre>

## Build package.

<pre>
makepkg
</pre>

## Install package.

<pre>
su
pacman --upgrade _package_name_.pkg.tar.xz
</pre>

Enjoy your new package form AUR repository :)

## Final word

If you plan to use AUR more frequently it's a good solution to use *yaourt*.
Checkout my post about yaourt.
