---
layout: post
title:  "systemd-networkd and LLMNR"
date:   2015-11-30 23:01:00
---

## Intro

LLMNR is for discovering names of hosts in local network or names of nspawn
containers. Cool thing of it is that you have to only set name of your hosts
as usual.

```sh
hostname mypc1
```

## Configuration

Configuration need to be done on each host.

In */etc/nsswitch.conf* modify line with "hosts:" to look like this:
```sh
hosts: files mymachines resolve myhostname
```

In */etc/systemd/resolved.conf* enable LLMNR by uncomment it.
```sh
LLMNR=yes
```

LLMNR have to be enabled on each network interface. This is exmple of wireless
interface configuration */etc/systemd/network/wireless.network*.

```sh
[Match]
Name=wlp2s0

[Network]
DHCP=ipv4
LLMNR=yes
```

This works if you have your network configured by systemd-networkd.

Enable and start services systemd-networkd and systemd-resolved.

## Verify your configuration

Ping your host from other machine by its name.

## Troubleshooting

Mounting nfs resources via x-systemd.automount as option in */etc/fstab* file
won't work.

Don't know the solution, yet.
