---
layout: post
title:  "Setup Rails-Emberjs environment"
date:   2015-12-29 22:39:00
---
## install rails

Optionally create .rbuy-version and .ruby-gemset files.

<pre>
mkdir project_name
cd project_name
rails new . --skip-bundle
</pre>

Edit Gemfile Remove/comment lines

<pre>
# gem 'coffee-rails', '~> 4.1.0'
# gem 'turbolinks'
</pre>

Add line

<pre>
gem 'ember-cli-rails'
</pre>

Now install dependencies.

<pre>
bundle install
</pre>

## Install Ember and whole javascript stack

### NVM

NVM - Node Version Manager is like Ruby's RVM.

<pre>
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.30.0/install.sh | bash
</pre>

Checkout for new version https://github.com/creationix/nvm

### install node.js

Checkout available versions.

<pre>
nvm ls-remote
</pre>

Install last version.

<pre>
nvm install 5.3
</pre>

Use node. It's Javascript you have to do such things manually ;-)
And prepare yourself. In Ruby world many tasks that can be accomplished
by computers are done by them. In Javascript world you are the prole.

<pre>
nvm use 5.3
</pre>

### install Emberjs

NPM - Node Package Manager is like Ruby's gem. It's part of node package.
With Ember.js there is another package manager - bower. Both are necessary.
On "ember new" command bower's dependencies are automatically installed.
You need to manually run "bower install" when you clone application.
In this case rake task takes care of bower dependencies.

<pre>
npm install -g ember-cli
</pre>

Create emberjs application.

<pre>
ember new frontend --skip-git
</pre>

Configure frontent app, you can use generator

<pre>
rails generate ember:init
</pre>

If this isn't work, do it manually. Create file *config/initializer/ember.rb*
<pre>
EmberCli.configure do |c|
  c.app :frontend
end
</pre>

<pre>
cd frontend
ember install ember-cli-rails-addon
</pre>

Mount Ember app in "/forntent" path (in this example), in file *config/routes.rb*

<pre>
mount_ember_app :frontend, to: '/frontent'
</pre>

In the '/frontend/config/environment.js' define same path as baseURL

<pre>
  baseURL: '/frontend',
</pre>

Install Ember's dependencies with rake.
You have to have bower installed.

<pre>
npm install -g bower
</pre>

<pre>
rake ember:install
</pre>

@TODO: Take care of tests :-)

## Troubleshooting

### ENOSPC

<pre>
ActionView::Template::Error (:frontend has failed to build: Error: watch /home/marikowa/project/rbem/frontend/app ENOSPC
</pre>

This means that there is no free space or more likely you've reached
ionotify limit.

Ad-hoc solution. As root run:

<pre>
sysctl fs.inotify.max_user_watches=100000
</pre>

Permanent solution. Add or increase parameter in file
/usr/lib/sysctl.d/50-default.conf

<pre>
fs.inotify.max_user_watches = 100000
</pre>
