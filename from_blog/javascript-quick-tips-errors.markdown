---
layout: post
title:  "JavaScript quick tips - errors"
date:   2016-01-23 15:01:00
---

## Catch the error.

{% highlight JavaScript %}
try {
  somethin_wrong()
} catch(error) {
  console.error("ERROR: " + error.name + ": " + error.message)
} finally {
  console.info("Bye...")
}

// -> ERROR: ReferenceError: somethin_wrong is not defined
//    Bye...
```

Finally is always executed.

{% highlight JavaScript %}
function ok(){
  console.info("everything is OK")
}

try {
  ok()
} catch(error) {
  console.error("ERROR: " + error.name + ": " + error.message)
} finally {
  console.info("Bye...")
}

// -> everything is OK
//    Bye...
```

### Warning

Different browsers generate different errors. You can't analyze name or message
properties of error passed to catch block.

## Define your own errors

{% highlight JavaScript %}
try {
  throw new Error('My own message of standard error')
} catch(error) {
  console.error("ERROR: " + error.name + ": " + error.message)
}
// -> ERROR: Error: My own message of standard error
```

There are other types of standard error: EvalError, RangeError, ReferenceError,
SyntaxError, TypeError and URIError.

{% highlight JavaScript %}
try {
  throw { name: 'Oops', message: 'Not good...' }
} catch(error) {
  console.error("ERROR: " + error.name + ": " + error.message)
}
// -> ERROR: Oops: Not good...
```
