---
layout: post
title:  "JavaScript quick tips - prototypes"
date:   2016-01-23 15:31:00
---

Each function has prototype attribute, prototype is an object, it can have
own attributes and methods. If function will be used as constructor, newly
created object can access to attributes and methods of the prototype
like owns.

Lets say we develop a game. I the game player has an avatar. We need object
for that avatar.
{% highlight JavaScript %}
function Avatar(name){
  this.name = name
  this.say_hello = function(){
    return "My name is " + this.name
  }
}

var player1 = new Avatar("Joe")

console.log(player1.say_hello())
```

We have very nice avatar, but players should have many avatars to chose from.
There are types of avatar, like pirate, knight, wizard, alchemist and princes.

Lets create knight avatar.

{% highlight JavaScript %}
function Knight(name){
  this.name = name
  this.character = "knight"
  this.say_hello = function(){
    return "My name is " + this.name
  }
  this.who_am_i = function(){
    return "I am a " + this.character
  }
}

var player2 = new Knight("Arthur")

console.log(player2.say_hello());
console.log(player2.who_am_i());
```

It's against DRY rule obviously. In normal programming languages we would just
use inheritance. But since we were forced to use JavaScript we have to use
prototypes.

{% highlight JavaScript %}
function Avatar(name){
  this.name = name
  this.say_hello = function(){
    return "My name is " + this.name
  }
}

var player1 = new Avatar("Joe")
console.log(player1.say_hello())

function Knight(name){
  this.name = name
  this.character = "knight"
  this.who_am_i = function(){
    return "I am a " + this.character
  }
}
Knight.prototype = new Avatar();

var player2 = new Knight("Arthur");
console.log(player2.say_hello());
console.log(player2.who_am_i());
```
