---
layout: post
title:  "Jekyll deployment with git"
date:   2015-12-06 15:24:00
---

## Assumptions

* Git flow in development process.
* Bare remote repository in ~/your_repo.
* Path to static content (Jekyll in the end is static) for http server /srv/your_site.
* tmp dir in home

## Git hook on server

```sh
branch=$(git rev-parse --symbolic --abbrev-ref $1)

if [ "$branch" == "master" ]; then
  GIT_REPO=$HOME/your_repo
  TMP_GIT_CLONE=$HOME/tmp/your_site
  PUBLIC_WWW=/srv/your_site

  if [ -d "$TMP_GIT_CLONE" ]; then
    git --git-dir=$TMP_GIT_CLONE/.git --work-tree=$TMP_GIT_CLONE pull origin master
  else
    git clone $GIT_REPO $TMP_GIT_CLONE
  fi
  jekyll build -s $TMP_GIT_CLONE -d $PUBLIC_WWW --future
fi

exit
```

On each push to master branch this hook will clone the repo to tmp dir or
push the changes if it already exists and generate with jekyll static content
on http server location. User have to have rights to write on /srv/your_site.

Jekyll must be installed on the system. I use system ruby and its gems.
You can install it with rvm or rbenv.

## Troubleshooting

If you have Jekyll installed as user's gem and you get error message like this

```sh
remote: hooks/post-update: line 18: jekyll: command not found
```

Check your shell environment variables.
