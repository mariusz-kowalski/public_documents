---
layout: post
title:  "JavaScript quick tips - functions"
date:   2016-01-08 18:10:00
---

## Parameters of functions.

Parameters are optional.

{% highlight JavaScript %}
function log_params(a, b, c){
  console.log(a,b,c)
}
log_params("x", "y")
// -> x y undefined
```

Not passed parameters have value undefined.

You can pass more parameters that there are defined.

{% highlight JavaScript %}
function log_params(a, b, c){
  console.log(a,b,c)
}
log_params("x", "y", "z", "q")
// -> x y z
```

It's pointless that way, but there is array of all parameters passed to
function.

{% highlight JavaScript %}
function log_params(a){
  console.log("a: ", a)
  console.log("all: ", arguments)
}
log_params("x", "y", "z", "q")
// -> a:  x
//    all:  ["x", "y", "z", "q"]
```

## What functions really are?

In JavaScript functions are variables.

{% highlight javascript %}
var f = function(){} // equivalent of: function f(){}
// -> undefined
typeof f
// -> "function"
```

## Scopes

Function creates new scope. Function's local scope inherits scope from
it's function scope.

{% highlight javascript %}
var global_var = "global"
function scoping(){
  var local_var = "local"
  return local_var + global_var
}

scoping()
// -> "localglobal"
local_var
// -> Uncaught ReferenceError: local_var is not defined
```

Always initialize variables with *var* keyword, it places variable in the scope.
Otherwise you create global variable.

{% highlight javascript %}
function do_something(){
  // Don't do that
  new_global_var = "some value"
}
```

Since functions are just variables there is possible to:

* create function inside function.

{% highlight javascript %}
var big = function(){
  var little = function(){
    // do something
  }
}
```

* pass function to another function as a parameter.

{% highlight javascript %}
var log_and_run = function(job){
  console.log("Result of the job is " + job())
}

log_and_run(function(){
  return Math.random() + 1
})
// -> Result of the job is 1.816971312975511
```

* return function as result of function.

{% highlight JavaScript %}
function period(){
  var beginning = Date.now()
  return function(){
    return (Date.now() - beginning) / 1000
  }
}
var message_period = period()
alert("How long this message is shown?")
console.log(message_period() + "seconds")
```

## Anonymous functions

Functions doesn't have to be named. Nameless function is anonymous function :)
Like those we've passed as a parameters by defining it in place.
However anonymous can be just created:

{% highlight JavaScript %}
(function(){
  console.log("What's my name?")
})
```

It's totally useless that way, because its code be never executed, unless we
execute it immediately.

{% highlight JavaScript %}
(function(){
  var message = "What's my name?"
  console.log(message)
}())
```

That way, this code will be run once and will not clutter name space, all of its
variables will be collected by garbage collector.

And version with parameter

{% highlight JavaScript %}
var player = "Jonh"
;(function(name){
  var message = "What's my name?"
  console.log(message)
  console.info(name)
}(player))
```

Semicolon at the beginning of second line is important, interpreter treats
first parenthesis as execution of function...

{% highlight JavaScript %}
console.log
("this is parameter of log function")
// -> this is parameter of log function
```

## Function remembers its scope

{% highlight JavaScript %}
function counter(){
  var count = 0
  return function(){
    console.info("executed " + count + (count == 1 ? " time" : " times") )
    count++
  }
}

var how_many_times = counter()

how_many_times()
how_many_times()
how_many_times()
console.log("and the count is: " + count)

// -> executed 0 times
//    executed 1 time
//    executed 2 times
//    Uncaught ReferenceError: count is not defined(…)
```

Anonymous function returned by counter and assigned to how_many_times variable
sees count variable (can change it) despite the fact how_many_times is
executed in scope where count isn't available. Function always remembers its
scope - where it was defined not where it's executed.
This mechanism is called **closure**.

## Functions - final word

There is no such thing as function in JavaScript. Function is an Object.
Check out next episode "JavaScript quick tips - objects" (soon). There is no
Arrays also.
