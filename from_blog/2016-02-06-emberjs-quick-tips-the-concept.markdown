---
layout: post
title:  "Emberjs quick tips - the concept"
date:   2016-02-06 17:28:00
---

object

## routes

example

app/router.js

{% highlight JavaScript %}
this.resource('users'); // -> /users
this.route('user.new', { path: '/users/new' }); // -> /users/new
this.resource('users', { path: 'users/:id' }, function () { // -> /users/1
  this.route('edit'); // -> /users/1/edit
})
```

Nested routes will nest templates, for example '/users/1/edit' will show
template users/1 and "inside it" edit. @FIXME: need more explanation...

## The flow.

When user open some page, lets say '/users', ember will try to instantiate
objects in order: App.UsersRoute, App.UsersController, App.UsersView and will
try to find template users.js.emblem (or users.hbs). If any (or some) of this
objects is not defined then ember will create some default versions of them.

Notice, UsersRoute has nothing to do with router. It's something different
@FIXME: confirm that :-)

## The Route.

Route is an object that is created when user enters the route (specified in
router). Route will call hooks in order: beforeModel, model,
afterModel, activate, setupController, deactivate. Hooks can be defined in the
route object.

Note for Rails developers: Route is little bit similar to Rails controller.

I don't fuck'n know what is this shit about. "model" probably is for getting
model/data but who knows what are requirements for this fucking hook.
