systemd-nspawn tips & tricks
============================

Executing commands in containers
--------------------------------

Running simple shell (it won't execute ~/.bashrc)

```bash
sudo machinectl shell _user_@_machine_
```

Run full environment shell

```bash
sudo machinectl shell _user_@_machine_ /bin/bash --login
```

Run some command in full environment.

```bash
sudo machinectl shell _user_@_machine_ /bin/bash --login -c "ls /home"
```

Preparing often used commands inside containers.

Create shell script (eg. my_srv) in home dir, it can be linked to /usr/bin
or /usr/local/bin if you want to use it in containers shell.

Command for running rails server

```bash
#!/bin/bash
/bin/bash --login -c 'cd /app/path; rails server --bind 0.0.0.0'
```

and then run it (you must give pull path)

```bash
sudo machinectl shell _user_@_machine_ /home/_user_/my_srv
```

Cloning machines
----------------

```bash
sudo machinectl clone _machine_name_ _new_name_
```

On btrfs filesystem clone machine will be created as an snapshot. Config file
in /etc/systemd/nspawn/_machine_name_.nspawn will be copied.

You need to change hostname of the clone (it is same as source machine).

```bash
sudo hostnamectl --machine=_new_name_ set-hostname _new_name_
```

You also need to change id of the clone. Simply remove from clone
_new_name_/etc/machine-id be sure that the machine is stop. System will generate
new one on next boot.

You can leave hostname (and probably machine-id) to have alternative of same
machine but you should prevent from running both orginal and it's clone.
Create config for clone.

```bash
sudo systemctl edit systemd-nspawn@_new_name_.sevice
```

It will create drop-in file and open it with your $editor. Place there
conflict parameter with name of original machine.

```
[Unit]
Conflicts=systemd-nspawn@_name_.service
```

And similar config for original machine.

```bash
sudo systemctl edit systemd-nspawn@_name_.sevice
```

```
[Unit]
Conflicts=systemd-nspawn@_new_name_.service
```

Transferring machines
---------------------

### btrfs send/receive over ssh

requirenments:
* machine on btrfs subvolume
* readonly snapshot of the machine

remarks:
* what about attr? They seems to be lost in the process... Yeah, rend/receive
  loses files attributes, it will be implemented in some future version.
* recreate NOCOW attribute

You can transfer machine over ssh with btrfs send/receive feature.

```bash
btrfs send _machine_snapshot_ |
gzip --stdout |
ssh remote_machine "cat - | gunzip --stdout | btrfs receive /var/lib/machines"
```

~~This will create readonly subvolume, to enable writing:~~

```bash
btrfs property set _machine_ ro false
```

This could be a bad idea, it maybe better if you create writable snapshot of
this subvolume...
```bash
btrfs subvolume snapshot _machine_ _rw_machine_
```

To reestablish NOCOW attribute (on mysql dir as an example), rename it
with _old postfix, make new mysql dir, set C attribute and copy content
to it with
--archive parameter. Remove old content.

```bash
mv mysql mysql_old
mkdir mysql
chattr +C mysql
cp --archive mysql_old/* mysql
rm -rf mysql_old
```

Next time you can send only incremental changes to this machine, but more about
this on next chapter

Ephemeral machine
-----------------

### Requirements

* btrfs filesystem

### Preparing

To easily run any existing machine in ephemeral mode create new unit for it by
coping original systemd-nspawn@.service to etc location.

```bash
sudo cp /usr/lib/systemd/system/systemd-nspawn@.service \
        /etc/systemd/system/systemd-nspawn-ephemeral@.service
```

And modify ExecStart from Service section:
* remove "-U" option
* remove "--link-journal=try-guest" option
* add "--ephemeral" option

At the end add section Exec with parameter "PrivateUsers = No"

Final version of file /etc/systemd/system/systemd-nspawn-ephemeral@.service

```
[Unit]
Description=Container %i
Documentation=man:systemd-nspawn(1)
PartOf=machines.target
Before=machines.target
After=network.target

[Service]
ExecStart=/usr/bin/systemd-nspawn \
  --quiet \
  --keep-unit \
  --boot \
  --network-veth \
  --settings=override \
  --machine=%i \
  --ephemeral
KillMode=mixed
Type=notify
RestartForceExitStatus=133
SuccessExitStatus=133
Slice=machine.slice
Delegate=yes
TasksMax=16384

# Enforce a strict device policy, similar to the one nspawn configures
# when it allocates its own scope unit. Make sure to keep these
# policies in sync if you change them!
DevicePolicy=closed
DeviceAllow=/dev/net/tun rwm
DeviceAllow=char-pts rw

# nspawn itself needs access to /dev/loop-control and /dev/loop, to
# implement the --image= option. Add these here, too.
DeviceAllow=/dev/loop-control rw
DeviceAllow=block-loop rw
DeviceAllow=block-blkext rw

[Install]
WantedBy=machines.target

[Exec]
PrivateUsers = No
```

### Running ephemeral machine

Original _machine_ must be stop due to veth name conflict (see next cap.).
To run _machine_ in ephemeral mode execute:

```bash
sudo systemctl start systemd-nspawn-ephemeral@_machine_
```

After power off the machine all changes made to it will be lost.
Machine is actually run on btrfs snapshot of _machine_ and the snapshot is
removed after machine is down.

### Running ephemeral simultaneously with original

> This section is under construction

Made further modification to unit file:

* add "directory=/var/lib/machines/%i" option
* modify "machine=eph-%i" option

This will change machine name, so it can't be drop-in replacement for it's
original machine, eg. database server which is identified by machine hostname
which is sam as machine name...

Consider making another ephemeral unit...
